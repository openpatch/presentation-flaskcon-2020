# OpenPatch an Example for Building Microservices with Flask

OpenPatch is an open platform for assessment and training of programming
skills. It is currently driven by twelve microservices. The frontend
microservice is implemented in React. Most backend microservices are
implemented in Flask.

In this session the challenges and solutions of building a microservice
architecture for OpenPatch with Flask will be presented. It will be shown how
authentication with JWT via a centralized authentification service is
implemented, how communication between the microservices with RabbitMQ is
integrated, how versioning of microservices (api-endpoints and
database-schemas) is used to keep backward-compatibility and what means are set
up to ease the development of new microservices.

This session should provide insights in the development of a microservice
architecture with Flask, which could help others developers to implement their
own or to spark the discussion of best practices.

## Run

```
yarn start
```

## Build
```
yarn build
```
