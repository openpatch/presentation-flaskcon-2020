import React from 'react';
import ReactDOM from 'react-dom';

import {
  Deck,
  Heading,
  Markdown,
  FlexBox,
  Slide,
  Notes,
  Box,
  Stepper,
  Link,
  Table,
  TableHeader,
  TableBody,
  TableRow,
  TableCell,
  UnorderedList,
  ListItem,
  Appear,
  Text,
  Image,
} from 'spectacle';

import Center from './components/Center';
import Speaker from './components/Speaker';
import References from './components/References';
import OpenPatch from './components/OpenPatch';
import Agenda from './components/Agenda';
import CodePaneCustom from './components/CodePaneCustom';
import Stamp from './components/Stamp';

import theme from './theme';
import template from './template';

const assets = {
  speaker: require('../assets/mike.png').default,
  openpatch: require('../openpatch.png').default,
  openpatchBanner: require('../assets/openpatch_banner.png').default,
  architecture: require('../assets/architecture.svg').default,
  authentification: require('../assets/authentification.svg').default,
  authentificationPossibilities: require('../assets/authentification_possibilities.svg')
    .default,
  authentificationGateway: require('../assets/authentification_gateway.svg')
    .default,
  authentificationOP: require('../assets/authentification_op.svg').default,
  authentificationTrust: require('../assets/authentification_trust.svg')
    .default,
  versioning: require('../assets/versioning.svg').default,
  communication: require('../assets/communication.svg').default,
  communicationComparision: require('../assets/communication_comparision.svg')
    .default,
  deployment: require('../assets/deployment.svg').default,
  infrastructurePipelines: require('../assets/infrastructure_pipelines.png')
    .default,
  website: require('../assets/website.svg').default,
  twitter: require('../assets/twitter.svg').default,
  github: require('../assets/github.svg').default,
  ansible: require('../assets/ansible.png').default,
  terraform: require('../assets/terraform.png').default,
  docker: require('../assets/docker.png').default,
  container: require('../assets/container.png').default,
  server: require('../assets/server.png').default,
};

const Presentation = () => (
  <Deck theme={theme} template={template} transitionEffect="fade">
    <Slide>
      <Heading>
        OpenPatch an Example for Building Microservices with Flask
      </Heading>
      <Center>
        <Speaker src={assets.openpatch} size={180} square marginRight={40} />
        <Speaker src={assets.speaker} size={200} />
      </Center>

      <Center flexDirection="column">
        <Text margin="0px">@mikebarkmin</Text>
        <Text margin="0px" fontSize={28}>
          Computer Science Education Researcher and Hobby Developer
        </Text>
      </Center>
      <References
        fontSize={36}
        references={[
          {
            title: 'https://www.barkmin.eu',
            href: 'https://www.barkmin.eu',
          },
        ]}
      />
    </Slide>
    <Slide>
      <Heading>Agenda</Heading>
      <Markdown>
        {`
- OpenPatch
- Architecture
- Authentification
- Communication
- Versioning
- Deployment
`}
      </Markdown>
    </Slide>
    <Slide>
      <FlexBox>
        <Image src={assets.openpatchBanner} />
      </FlexBox>
    </Slide>
    <Slide>
      <Appear elementNum={0}>
        <Stamp rotate="-30deg">non public beta</Stamp>
      </Appear>
      <OpenPatch />
      <References
        references={[
          {
            href: 'https://gitlab.com/openpatch',
            title: 'GitLab',
          },
          {
            href: 'https://openpatch.app',
            title: 'Website',
          },
          {
            href: 'https://twitter.com/openpatchorg',
            title: 'Twitter',
          },
        ]}
      />
    </Slide>
    <Slide>
      <Heading>Architecture</Heading>
      <FlexBox>
        <FlexBox flex={1}>
          <Markdown>
            {`
- Microservices
- Technologies
  - MariaDB
  - Python/Flask
  - JS/React/Next
  - RabbitMQ
  - Docker
              `}
          </Markdown>
        </FlexBox>
        <FlexBox flex={1}>
          <Image src={assets.architecture} height={500} />
        </FlexBox>
      </FlexBox>
    </Slide>
    <Slide>
      <Agenda step={0} assets={assets} />
      <Notes>
        <Markdown>{`
- Time: 3 Min
          `}</Markdown>
      </Notes>
    </Slide>
    <Slide>
      <Heading>Authentification!?</Heading>
      <Stepper defaultValue={[]} values={[0, 1, 2]}>
        {(value) => (
          <FlexBox>
            <Box p={20}>
              <Image
                style={{
                  opacity: value !== 0 ? 0.2 : 1,
                }}
                src={assets.authentificationGateway}
                width="100%"
              />
            </Box>
            <Box p={20}>
              <Image
                style={{
                  opacity: value !== 1 ? 0.2 : 1,
                }}
                src={assets.authentificationTrust}
                width="100%"
              />
            </Box>
            <Box p={20}>
              <Image
                style={{
                  opacity: value !== 2 ? 0.2 : 1,
                }}
                src={assets.authentificationOP}
                width="100%"
              />
            </Box>
          </FlexBox>
        )}
      </Stepper>
      <Notes>
        <Markdown>{`
- The most important service
- One Authentification Service
- Flask-JWT-Extended
          `}</Markdown>
      </Notes>
    </Slide>
    <Slide>
      <Heading>Auth Service: Core Endpoints</Heading>
      <Markdown>{`
- based on flask_jwt_extended
- **/login**: create access and refresh tokens
- **/register**: new users, can be switched to invite only
- **/refresh**: create new access tokens
- **/verify**: checks a access token
- **/logout**: blacklists a refresh token

        `}</Markdown>
      <References
        references={[
          {
            href: 'https://gitlab.com/openpatch/authentification-backend',
            title: 'Authentification Service (GitLab)',
          },
        ]}
      />
    </Slide>
    <Slide>
      <Heading>Auth Service: Other Endpoints</Heading>
      <Markdown>{`
- **/reset-password**: sends email with reset password link
- **/resend-email**: resents confirm email
- **/confirm-email**: confirm a email based on a token
- **/new-policies**: checks if new policies need to be accepted
        `}</Markdown>
      <References
        references={[
          {
            href: 'https://gitlab.com/openpatch/authentification-backend',
            title: 'Authentification Service (GitLab)',
          },
        ]}
      />
    </Slide>
    <Slide>
      <Heading>Authentification for other Services</Heading>
      <Stepper
        defaultValue={[]}
        values={[
          [2, 15],
          [16, 22],
        ]}
      >
        {(value) => (
          <CodePaneCustom
            language="python"
            highlightStart={value[0]}
            highlightEnd={value[1]}
          >
            {`
# openpatch_core.jwt
def jwt_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        base_url = os.getenv("OPENPATCH_AUTHENTIFICATION_SERVICE")
        if base_url:
            headers = {"Authorization": request.headers.get("Authorization", None)}
            url = "%s/v1/verify" % base_url
            r = requests.get(url, headers=headers)
            if r.status_code != 200:
                abort(401)
        return func(*args, **kwargs)
    return wrapper

# assessment-service
from openpatch_core.jwt import jwt_required, get_jwt_claims

@app_route("/stuff", methods=["GET"])
@jwt_required
def get_stuff():
    claims = get_jwt_claims()
          `}
          </CodePaneCustom>
        )}
      </Stepper>
      <References
        references={[
          {
            href:
              'https://gitlab.com/openpatch/flask-core/-/tree/master/openpatch_core/jwt',
            title: 'JWT Module (GitLab)',
          },
        ]}
      />
    </Slide>
    <Slide>
      <Agenda step={1} assets={assets} />
      <Notes>
        <Markdown>{`
- Time: 8 Min
          `}</Markdown>
      </Notes>
    </Slide>
    <Slide>
      <Heading>Communication</Heading>
      <FlexBox>
        <FlexBox flex={1}>
          <Markdown>{`
- sync with http (requests)
  - e.g. authentification
- async with amqp (pika)
  - e.g. mails, activities
            `}</Markdown>
        </FlexBox>
        <FlexBox flex={1}>
          <Image src={assets.communicationComparision} height={230} />
        </FlexBox>
      </FlexBox>
      <Notes>
        <Markdown>{`
- You have already seen one communication pattern direct http calls
- HTTP -> requests (sync)
- AMQP -> pika (async)
  - Consumer
  - Producer
- What when you need to change a microservice? Does the communication break?
          `}</Markdown>
      </Notes>
      <References
        references={[
          {
            href: 'https://github.com/pika/pika',
            title: 'Pika (GitHub)',
          },
        ]}
      />
    </Slide>
    <Slide>
      <Heading>Communication - HTTP</Heading>
      <CodePaneCustom language="python" fontSize={24} autoFillHeight>
        {`
import requests 
from flask import request

@app.route("/protected", method=["GET"])
def protected():
  headers = {"Authorization": request.headers.get("Authorization", None)}
  url = "%s/v1/verify" % os.getenv("AUTH_SERVICE_URL")
  r = requests.get(url, headers=headers)
  if r.status_code != 200:
      abort(401) # not authorized

  return "access"
        `}
      </CodePaneCustom>
      <Notes>
        <Markdown>
          {`
- Simple HTTP communication
- Pass Authorization Header, which contains a jwt token
- If everything is fine grant access to the user and return
- Of course I use the decorator as shown early for checking the jwt token, this
is just for demonstration purposes
            `}
        </Markdown>
      </Notes>
    </Slide>
    <Slide>
      <Heading>Communication - AMQP</Heading>
      <Stepper
        defaultValue={[]}
        values={[
          [1, 8],
          [10, 15],
        ]}
      >
        {(value) => (
          <CodePaneCustom
            language="python"
            highlightStart={value[0]}
            highlightEnd={value[1]}
          >
            {`from openpatch_core.rabbitmq import RabbitMQ

rabbit_mq = RabbitMQ(app)

@rabbit_mq.queue(queue_name="activities_queue", exchange_name="activities_exchange")
def consume_activities(ch, method, props, body):
    ... # validate
    save(body)

@app.route(/comment, method=["POST"])
def post_comment():
  ... # validate
  rabbit_mq.publish(comment,
                    exchange_name="activities_exchange",
                    routing_key="activities_queue")
        `}
          </CodePaneCustom>
        )}
      </Stepper>
      <References
        references={[
          {
            href:
              'https://gitlab.com/openpatch/flask-core/-/tree/master/openpatch_core/rabbitmq',
            title: 'RabbitMQ Module (GitLab)',
          },
          {
            href: 'https://www.youtube.com/watch?v=deG25y_r6OY',
            title: 'RabbitMQ in 5 Minuted (YouTube)',
          },
        ]}
      />
    </Slide>
    <Slide>
      <Agenda step={2} assets={assets} />
      <Notes>
        <Markdown>{`
- Time: 12 Min
          `}</Markdown>
      </Notes>
    </Slide>
    <Slide>
      <Markdown>
        {`
## MAJOR.MINOR.PATCH
- Commit Subject
  - feat => MINOR+1, fix => PATCH+1, docs, test, ...

- Commit Footer
  - BREAKING CHANGE => MAJOR+1

- e.g.: 1.0.0 -> "feat: add avatar to member" -> 1.1.0
          `}
      </Markdown>
      <Notes>
        <Markdown>
          {`
- All projects/services follow the semantic versioning 2.0.0 guidelines
- Changelogs and release will be automatically created. No human interaction needed.
- But git commit messages need to have a certain style.
- This is just the schema. Lets see how a flask service is versioned to keep backwards capability.
          `}
        </Markdown>
      </Notes>
      <References
        references={[
          {
            href: 'https://semver.org/',
            title: 'Semantic Versioning 2.0.0',
          },
          {
            href: 'https://github.com/semantic-release/semantic-release',
            title: 'Semantic Release',
          },
          {
            href:
              'https://gitlab.com/openpatch/gitlab-ci-pipeline/-/blob/master/build-and-release.yml',
            title: 'CI Pipeline: Build and Release (GitLab)',
          },
        ]}
      />
    </Slide>
    <Slide>
      <Heading>Example Service</Heading>
      <Stepper
        defaultValue={[]}
        values={[
          [2, 12],
          [15, 17],
          [4, 10],
          [11, 13],
          [7, 7],
          [8, 10],
          [5, 5],
        ]}
      >
        {(value) => (
          <CodePaneCustom highlightStart={value[0]} highlightEnd={value[1]}>
            {`openpatch_example
├── api
│   ├── __init__.py
│   ├── v1
│   │   ├── errors.py <-- Error Codes
│   │   ├── __init__.py
│   │   ├── samples.py <-- API Endpoints
│   │   └── schemas <-- Marshmallow Schemas
│   │       ├── __init__.py
│   │       └── sample.py
│   └── v2
│       ├── ...
│           
├── __init__.py
└── models <-- SQLAlchemy Models
    ├── __init__.py
    └── sample.py`}
          </CodePaneCustom>
        )}
      </Stepper>
      <References
        references={[
          {
            href:
              'https://gitlab.com/openpatch/microservice-versioning-example',
            title: 'Microservice Versioning Example (GitLab)',
          },
        ]}
      />
      <Notes>
        <Markdown>{`
- Eventhough you are trying to decouple services as much as possible, sometimes they need to communicate to each other
- Therefore keeping backwards-compatability is important
- Unittest are very important
- Routes prefixed with v1/v2 (blueprints)
- SQLAlchemy and Alembic
- Marshmallow-SQLAlchemy
- Marshmallow
          `}</Markdown>
        <Markdown>
          {`
- Lets assume we have a service already build
- It looks like this
- 
            `}
        </Markdown>
      </Notes>
    </Slide>
    <Slide>
      <Heading>Example Service</Heading>
      <Stepper
        defaultValue={[]}
        values={[
          [2, 6],
          [9, 20],
          [22, 25],
          [27, 34],
        ]}
      >
        {(value) => (
          <CodePaneCustom
            language="python"
            highlightStart={value[0]}
            highlightEnd={value[1]}
          >
            {`
# api.v1.sample
@api.route(base_url, methods=["GET"])
def get_samples():
    samples = Sample.query.all()
    return jsonify(SAMPLES_SCHEMA.dump(samples))


@api.route(base_url, methods=["POST"])
def add_sample():
    sample_json = request.get_json()
    try:
        sample = SAMPLE_SCHEMA.load(sample_json, session=db.session)
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    db.session.add(sample)
    db.session.commit()

    return jsonify({"status": "ok", "sample_id": sample.id})

# models.sample
class Sample(Base):
    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    sample_name = db.Column(db.Text())

# api.v1.schemas.sample
class SampleSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Sample
        load_instance = True

    id = fields.UUID()
    sample_name = ma.auto_field()
        `}
          </CodePaneCustom>
        )}
      </Stepper>
    </Slide>
    <Slide>
      <Markdown>
        {`
# Changes for 2.0.0
- Feat: add value to sample
- Breaking Change: Return json object instead of an array
- Breaking Change: rename sample_name to name
        `}
      </Markdown>
      <CodePaneCustom language="diff">
        {`"sample": {
  "id": UUID,
- "sample_name": String,
+ "name": String,
+ "value": Integer
}`}
      </CodePaneCustom>
      <Notes>
        <Markdown>
          {`
- Eventhough we are looking at the database model, we should think in terms of external api
- They ar tightly connected in this example
- We not only want to change our internal database model
- We also would like to change the external api
            `}
        </Markdown>
      </Notes>
    </Slide>
    <Slide>
      <Heading>Change Database Model</Heading>
      <CodePaneCustom language="diff">
        {`class Sample(Base):
    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
-   sample_name = db.Column(db.Text())
+   name = db.Column(db.Text())
+   value = db.Column(db.Integer())
      `}
      </CodePaneCustom>
      <Appear elementNum={0}>
        <Stamp rotate="-30deg">Breaks older versions</Stamp>
      </Appear>
      <Notes>
        <Markdown>
          {`
- Database needs to be extended
- Database reflects our most recent api
- Changes to the database will break older versions
- Leaving sample_name solves this, but leads to redundancies and other issues
          `}
        </Markdown>
      </Notes>
    </Slide>
    <Slide>
      <Heading>TODO</Heading>
      <Markdown>{`
- Fix older versions (v1)
- Implement new version (v2)
      `}</Markdown>
    </Slide>
    <Slide>
      <Heading>Fix v1</Heading>
      <CodePaneCustom language="diff">
        {`
# api.v1.schemas.sample
class SampleSchema(ma.SQLAlchemySchema):
     load_instance = True

     id = fields.UUID()
-    sample_name = ma.auto_field()
+    sample_name = ma.auto_field(column_name="name")

# tests.v1.test_samples
class TestSample(BaseTest):
     ...
-    self.assertEqual(sample.sample_name, "Hu")
+    self.assertEqual(sample.name, "Hu")
        `}
      </CodePaneCustom>
      <Notes>
        <Markdown>{`
- Marshmallow Schemas need to be adapted
- Tests might break, depending how you write them
          `}</Markdown>
      </Notes>
    </Slide>
    <Slide>
      <Heading>Implement v2</Heading>
      <Stepper
        values={[
          [2, 6],
          [9, 21],
          [24, 32],
        ]}
        defaultValue={[]}
      >
        {(value) => (
          <CodePaneCustom
            language="python"
            highlightStart={value[0]}
            highlightEnd={value[1]}
          >{`
# api.v2.sample
@api.route(base_url, methods=["GET"])
def get_samples():
    samples = Sample.query.all()
    return jsonify({"samples": SAMPLES_SCHEMA.dump(samples)}) # feat: return json object


@api.route(base_url, methods=["POST"])
@jwt_required
def add_sample():
    sample_json = request.get_json()
    try:
        sample = SAMPLE_SCHEMA.load(sample_json, session=db.session)
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    db.session.add(sample)
    db.session.commit()

    return jsonify({"status": "ok", "sample_id": sample.id})


# api.v2.schemas.samples
class SampleSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Sample
        load_instance = True

    id = fields.UUID()
    name = ma.auto_field()
    value = ma.auto_field()
        `}</CodePaneCustom>
        )}
      </Stepper>
    </Slide>
    <Slide>
      <Heading>Register v2</Heading>
      <Markdown>
        {`
- API versions are implemented as blueprints
        `}
      </Markdown>
      <CodePaneCustom language="diff">{`
    app.register_blueprint(api_v1, url_prefix="/v1")
+   app.register_blueprint(api_v2, url_prefix="/v2")
        `}</CodePaneCustom>
    </Slide>
    <Slide>
      <Heading>Versioning: Open Question</Heading>
      <UnorderedList>
        <ListItem>Should you version everything at once?</ListItem>
        <Appear elementNum={0}>
          <UnorderedList>
            <ListItem>v2 will have (all) endpoints v1 has</ListItem>
            <ListItem>v1/login v1/register v2/login v2/register</ListItem>
          </UnorderedList>
        </Appear>
        <ListItem>
          Or just the endpoints, which produce breaking changes?
        </ListItem>
        <Appear elementNum={1}>
          <UnorderedList>
            <ListItem>v2 will only have changed endpoints</ListItem>
            <ListItem>v1/login v1/register v2/register</ListItem>
          </UnorderedList>
        </Appear>
      </UnorderedList>
      <Appear elementNum={2}>
        <Stamp rotate="-30deg">COPY</Stamp>
      </Appear>
    </Slide>
    <Slide>
      <Agenda step={3} assets={assets} />
      <Notes>
        <Markdown>{`
- Time: 19 Min
          `}</Markdown>
      </Notes>
    </Slide>
    <Slide>
      <Heading>Infrastructure as Code</Heading>
      <Appear elementNum={0}>
        <Box position="absolute" width={200} height={200} left={140} top={300}>
          <Image src={assets.docker} height="100%" />
        </Box>
      </Appear>
      <Appear elementNum={1}>
        <Box
          position="absolute"
          width={200}
          height={200}
          left={0}
          right={0}
          margin="0 auto"
          top={300}
        >
          <Image src={assets.terraform} height="100%" />
        </Box>
      </Appear>
      <Appear elementNum={3}>
        <Box position="absolute" width={100} height={200} left={580} top={500}>
          <Image src={assets.server} width="100%" />
        </Box>
        <Box position="absolute" width={100} height={200} left={700} top={500}>
          <Image src={assets.server} width="100%" />
        </Box>
      </Appear>
      <Appear elementNum={4}>
        <Box position="absolute" width={100} height={100} top={200} right={200}>
          <Image src={assets.container} width="100%" />
        </Box>
        <Box position="absolute" width={100} height={100} top={350} right={40}>
          <Image src={assets.container} width="100%" />
        </Box>
        <Box position="absolute" width={100} height={100} top={350} right={380}>
          <Image src={assets.container} width="100%" />
        </Box>
        <Box position="absolute" width={100} height={100} top={500} right={200}>
          <Image src={assets.container} width="100%" />
        </Box>
      </Appear>
      <Appear elementNum={2}>
        <Box position="absolute" width={200} height={200} right={140} top={300}>
          <Image src={assets.ansible} height="100%" />
        </Box>
      </Appear>
      <References
        references={[
          {
            href: 'https:/5gitlab.com/openpatch/flask-microservice-base',
            title: 'Base Docker Image (GitLab)',
          },
          {
            href: 'https://gitlab.com/openpatch/infrastructure',
            title: 'Infrastructure (GitLab)',
          },
        ]}
      />
      <Notes>
        <Markdown>{`
- Everything is a docker container
- Server are provisioned with terraform
- A docker swarm cluster is deploy with ansible
- Services are deployed to the cluster with ansible
- Once a weak everything gets checked for updates
- It is done automatically with GitLab CI Pipelines
          `}</Markdown>
      </Notes>
    </Slide>
    <Slide>
      <Heading>Deployment</Heading>
      <FlexBox>
        <Image src={assets.infrastructurePipelines} />
      </FlexBox>
      <References
        references={[
          {
            href: 'https://gitlab.com/openpatch/infrastructure',
            title: 'Infrastructure (GitLab)',
          },
        ]}
      />
    </Slide>

    <Slide>
      <Heading>Questions?</Heading>
      <Table textAlign="center" px={150} mt={50}>
        <TableHeader>
          <TableRow>
            <TableCell textAlign="center">OpenPatch</TableCell>
            <TableCell></TableCell>
            <TableCell textAlign="center">Mike</TableCell>
          </TableRow>
        </TableHeader>
        <TableBody>
          <TableRow>
            <TableCell textAlign="center">
              <Speaker
                src={assets.openpatch}
                size={60}
                mb={50}
                mx="auto"
                square
              />
            </TableCell>
            <TableCell></TableCell>
            <TableCell textAlign="center">
              <Speaker src={assets.speaker} size={60} mb={50} mx="auto" />
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell textAlign="center">
              <Link href="https://www.openpatch.app">openpatch.app</Link>
            </TableCell>
            <TableCell textAlign="center">
              <Image src={assets.website} height={50} />
            </TableCell>
            <TableCell textAlign="center">
              <Link href="https://www.barkmin.eu">barkmin.eu</Link>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell textAlign="center">
              <Link href="https://twitter.com/openpatchorg">OpenPatchOrg</Link>
            </TableCell>
            <TableCell textAlign="center">
              <Image src={assets.twitter} height={50} />
            </TableCell>
            <TableCell textAlign="center">
              <Link href="https://twitter.com/mikebarkmin">mikebarkmin</Link>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell textAlign="center">
              <Link href="https://gitlab.com/openpatch">OpenPatch</Link>
            </TableCell>
            <TableCell textAlign="center">
              <Image src={assets.github} height={50} />
            </TableCell>
            <TableCell textAlign="center">
              <Link href="https://github.com/mikebarkmin">mikebarkmin</Link>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Slide>
  </Deck>
);

ReactDOM.render(<Presentation />, document.getElementById('root'));
