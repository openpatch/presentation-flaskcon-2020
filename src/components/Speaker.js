import React from 'react';
import { Box } from 'spectacle';
import theme from '../theme';

const Speaker = ({ src, name, size, square, ...props }) => {
  return (
    <Box
      style={{
        boxShadow: theme.shadows[8],
        borderRadius: square ? null : '50%',
        overflow: 'hidden',
        width: size,
        height: size,
      }}
      {...props}
    >
      <img src={src} width={size} alt={name} />
    </Box>
  );
};

export default Speaker;
