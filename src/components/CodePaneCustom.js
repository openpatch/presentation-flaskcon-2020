import React from 'react';
import { CodePane } from 'spectacle';
import theme from 'prism-react-renderer/themes/nightOwl';

function CodePaneCustom(props) {
  return <CodePane {...props} />;
}

CodePaneCustom.defaultProps = {
  fontSize: 24,
  theme: theme,
  autoFillHeight: true,
};

export default CodePaneCustom;
