import React from 'react';
import { FlexBox } from 'spectacle';

const Center = ({ children, ...props }) => (
  <FlexBox justifyContent="center" alignItems="center" {...props}>
    {children}
  </FlexBox>
);

export default Center;
