import React from 'react';
import { Grid, FlexBox, Text } from 'spectacle';

const features = [
  {
    title: 'Interactive Tasks',
    video: 'https://f002.backblazeb2.com/file/openpatch-static/interactive.mp4',
    area: 'a',
  },
  {
    title: 'Adaptive Tests',
    video: 'https://f002.backblazeb2.com/file/openpatch-static/test.mp4',
    area: 'b',
  },
  {
    title: 'Social Interactions',
    video: 'https://f002.backblazeb2.com/file/openpatch-static/social.mp4',
    area: 'e',
  },
  {
    title: 'Assessments',
    video: 'https://f002.backblazeb2.com/file/openpatch-static/assessment.mp4',
    area: 'd',
  },
  {
    title: 'Recordings',
    video: 'https://f002.backblazeb2.com/file/openpatch-static/player.mp4',
    area: 'c',
  },
];

function Feature({ title, video, area }) {
  return (
    <Grid gridArea={area}>
      <Text fontSize={24} textAlign="center">
        {title}
      </Text>
      <video width="100%" autoPlay loop>
        <source src={video} />
      </video>
    </Grid>
  );
}

function OpenPatch() {
  return (
    <Grid
      gridTemplateColumns="1fr 1fr 1fr"
      gridTemplateRows="1fr 1fr"
      gridGap={40}
      gridTemplateAreas={`"a b c"
      "d e c"`}
    >
      {features.map((f) => (
        <Feature key={f.area} {...f} />
      ))}
    </Grid>
  );
}

export default OpenPatch;
