import React from 'react';
import { Link, FlexBox } from 'spectacle';

function References({ references, fontSize }) {
  return (
    <FlexBox position="absolute" bottom={40} left={20} right={20}>
      {references.map(({ href, title }) => (
        <Link key={href} fontSize={fontSize} href={href}>
          {title}
        </Link>
      ))}
    </FlexBox>
  );
}

References.defaultProps = {
  references: [],
  fontSize: 20,
};

export default References;
