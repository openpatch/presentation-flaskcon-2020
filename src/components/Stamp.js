import React from 'react';
import { Text } from 'spectacle';

function Stamp({ rotate, children, ...props }) {
  return (
    <Text
      {...props}
      style={{
        position: 'absolute',
        top: '50%',
        left: 0,
        right: 0,
        textAlign: 'center',
        fontWeight: 900,
        transform: rotate ? `rotate(${rotate})` : null,
        width: 500,
        color: 'red',
        border: '10px solid red',
        background: 'white',
        borderRadius: 8,
        margin: '0 auto',
      }}
    >
      {children}
    </Text>
  );
}

Stamp.defaultProps = {
  rotate: null,
};

export default Stamp;
