import React from 'react';
import {
  Heading,
  Image,
  Table,
  TableHeader,
  TableRow,
  TableCell,
  TableBody,
} from 'spectacle';

function Agenda({ assets, step }) {
  return (
    <>
      <Heading>How does it work?</Heading>
      <Table>
        <TableHeader>
          <TableRow>
            <TableCell style={{ opacity: step != 0 ? 0.1 : null }}>
              Authentification
            </TableCell>
            <TableCell style={{ opacity: step != 1 ? 0.1 : null }}>
              Communication
            </TableCell>
            <TableCell style={{ opacity: step != 2 ? 0.1 : null }}>
              Versioning
            </TableCell>
            <TableCell style={{ opacity: step != 3 ? 0.1 : null }}>
              Deployment
            </TableCell>
          </TableRow>
        </TableHeader>
        <TableBody>
          <TableRow>
            <TableCell>
              <Image
                style={{ opacity: step != 0 ? 0.1 : null }}
                src={assets.authentification}
              />
            </TableCell>
            <TableCell>
              <Image
                style={{ opacity: step != 1 ? 0.1 : null }}
                src={assets.communication}
              />
            </TableCell>
            <TableCell>
              <Image
                style={{ opacity: step != 2 ? 0.1 : null }}
                src={assets.versioning}
              />
            </TableCell>
            <TableCell>
              <Image
                style={{ opacity: step != 3 ? 0.1 : null }}
                src={assets.deployment}
              />
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </>
  );
}

export default Agenda;
