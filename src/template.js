import React from 'react';
import { Link, FlexBox, Box, FullScreen, Progress } from 'spectacle';
import logo from '../openpatch.png';

export default () => (
  <>
    <FlexBox
      justifyContent="space-between"
      position="absolute"
      bottom={0}
      width={1}
    >
      <FlexBox padding="0 1em">
        <FullScreen />
      </FlexBox>
      <Box padding="1em">
        <Progress />
      </Box>
    </FlexBox>
    <FlexBox>
      <img
        src={logo}
        style={{
          position: 'absolute',
          top: 10,
          right: 10,
          width: 40,
          height: 40,
        }}
      />
    </FlexBox>
  </>
);
